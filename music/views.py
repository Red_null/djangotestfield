from django.shortcuts import render
from django.views import generic
from .models import Music, Group, Person


# Create your views here.

class IndexView(generic.ListView):
    model = Music
    template_name = 'music/index.html'
    context_object_name = 'music'

    def get_queryset(self):
        return Music.objects.all()
