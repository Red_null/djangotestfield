from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=250)
    surname = models.CharField(max_length=250)

    @property
    def full_name(self):
        return self.name + self.surname


class Group(models.Model):
    name = models.CharField(max_length=250)
    members = models.ManyToManyField(Person)


class Music(models.Model):
    track_name = models.CharField(max_length=250)
    time = models.FloatField(max_length=3)
    author = models.ForeignKey(Person, on_delete=models.SET_NULL, null=True)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return 'Name: {}, time {}'.format(self.track_name, self.time)
