import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from tst.models import Choice, Question


def createQuestion(text, date=timezone.now()):
    q = Question(text=text, date=date)
    return q


def createChoice(mark, question=createQuestion('TestQuestion'), date=timezone.now()):
    c = question.choice_set.create(mark=mark, date=date)
    return c


def postNewChoice(client, mark, questionId):
    return client.post(reverse('tst:addChoice', args={questionId}), {'mark': -1})


class ChoiceModuleTests(TestCase):

    def testCheckIncorrectDates(self):
        choice = Choice(date=timezone.now() + datetime.timedelta(days=3))
        self.assertIs(choice.isRecent(), False)


class ChoiceViewTests(TestCase):
    def testNoMarkCase(self):
        question = createQuestion('Hey')
        question.save()
        response = self.client.get(reverse('tst:question', args={question.id}))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'No marks')

    def testWrongArguments(self):
        question = createQuestion('hey')
        question.save()
        response = postNewChoice(self.client, -1, question.id)
        self.assertEquals(response.status_code, 400)
        response = postNewChoice(self.client, 6, question.id)
        self.assertEquals(response.status_code, 400)
