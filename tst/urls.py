from django.urls import path
from . import views

app_name = 'tst'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('question/<int:id>', views.question, name='question'),
    path('question/<int:id>/addChoice', views.addChoice, name='addChoice'),
    path('question/addQuestion', views.addQuestion, name='addQuestion'),
    path('question/<int:id>/update', views.updateQuestion, name='updateQuestion'),
    path('question/<int:id>/delete', views.deleteQuestion, name='deleteQuestion'),
]
