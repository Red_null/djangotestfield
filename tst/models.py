from django.db import models
from django.utils import timezone


# Create your models here.


class Question(models.Model):
    text = models.CharField(max_length=200)
    date = models.DateTimeField()

    def __str__(self):
        return "\"{}\" at: {}".format(self.text, self.date.ctime())

    def countChooses(self):
        return self.choice_set.count()

    countChooses.short_description = 'Chooses'

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    mark = models.SmallIntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return "Question: {}, {}* at: {}".format(self.question, self.mark, self.date.ctime())

    def isRecent(self):
        now = timezone.now()
        if now - timezone.timedelta(days=1) <= self.date <= now:
            return True
        else:
            return False

    isRecent.boolean = True
    isRecent.short_description = 'recent'
