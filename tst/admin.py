from django.contrib import admin

from .models import Question, Choice


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('text', 'date', 'countChooses')

    fieldsets = [
        (None, {'fields': ['text']}),
        ('Time info', {'fields': ['date'], 'classes': ['collapse']})
    ]
    inlines = [ChoiceInline]


class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('question', 'mark', 'date', 'isRecent')
    fields = ['question', 'mark', 'date']
    list_filter = ['mark', 'question']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
