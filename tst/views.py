from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseBadRequest
from django.utils import timezone
from django.urls import reverse
from django.views import generic

from tst.models import Question


class IndexView(generic.ListView):  # generic view
    template_name = 'tst/index.html'
    context_object_name = 'questions'

    def get_queryset(self):
        return Question.objects.all()


# def index(request):   # regular view
#     questions = Question.objects.all()
#     result = {
#         'questions': questions
#     }
#     return render(request, "tst/index.html", result)

def question(request, id):
    question = get_object_or_404(Question, id=id)
    chooses = question.choice_set.all().order_by('date')
    user = request
    result = {
        'question': question,
        'chooses': chooses
    }
    return render(request, "tst/question.html", result)


def addChoice(request, id):
    mark = int(request.POST['mark'])
    if mark < 0 or mark > 5:
        return HttpResponseBadRequest('Wrong mark')

    question = get_object_or_404(Question, id=id)
    question.choice_set.create(mark=mark, date=timezone.now())
    return redirect(reverse('tst:question', args={id}))


def addQuestion(request):
    question = Question()
    question.date = timezone.now()
    question.text = request.POST['text']
    question.save()
    return redirect(reverse('tst:index'))


def updateQuestion(request, id):
    question = get_object_or_404(Question, id=id)
    newText = request.POST['text']
    if newText:
        question.text = newText
        question.date = timezone.now()
        question.save()
        return redirect(reverse('tst:index'))
    else:
        return HttpResponseBadRequest('Wrong input')


def deleteQuestion(request, id):
    question = get_object_or_404(Question, id=id)
    question.choice_set.all().delete()
    question.delete()
    return redirect(reverse('tst:index'))
